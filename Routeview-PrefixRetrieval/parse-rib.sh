#!/bin/bash

# This script computes the start and end IP for a give prefix

if [ $# -ne 2 ]; then
   echo "USAGE: ./parse-rib.sh input_rib_human_readable_file outfile"
   exit 1
fi

#parse rib file and extrace prefix in dotted quad format with subnetmask (X.X.X.X/Y)
cat $1 | awk -F'|' '{print $6}'|uniq  > temp.txt


#read each prefix one at a time and compute IP range for that prefix
while read line; do

	# create array containing network address and subnet
	network=(${line//\// })

	if [ ${network[1]} -lt 16 ] ; then 
		continue
	fi

	# split network address by dot
	iparr=(${network[0]//./ })

	#convert network address in decimal format
	ipLong=$((${iparr[0]}*2**24 + ${iparr[1]}*2**16 + ${iparr[2]}*2**8 + ${iparr[3]}))

	#convert subnetmask  into decimal format along with its inverse
	ipMaskLong=0
	inverseIpMaskLong=0

	for i in `seq 1 32`;
	do
		j=$(( 1<<(32 - i) ))
		k=$(( 0<<(32 - i) ))
		if [ $i -le ${network[1]} ] ; then
			ipMaskLong=$(( ipMaskLong + j ))
			inverseIpMaskLong=$(( inverseIpMaskLong + k )) 
		else
			ipMaskLong=$(( ipMaskLong + k ))
			inverseIpMaskLong=$(( inverseIpMaskLong + j )) 
		fi
	       
	done    

	#network address in decimal format should be same as ipLong 
	#but if not then computes the correct network address
	netWorkadd=$(( ipLong & ipMaskLong ))

	#start IP address
	start=$(( netWorkadd + 1 ))

	#end IP address
	end=$((  $(( netWorkadd | inverseIpMaskLong )) - 1 ))

	echo $start'|'$end'|'${network[1]} >>$2
done < temp.txt


rm temp.txt

cat $2 |sort -R >temp
mv temp $2
