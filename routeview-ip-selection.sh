#!/bin/bash
set -u

#version number
VERSION=1.1.5

#Maximum sequential IP tried
MAXSEQ=${MAXSEQ-50}

# Scamper packets per second rate 1<=pps<=1000
PPS=${PPS-1000}

# Directory to temporarily store traceroute results
RESULTS=${RESULTS-"Traceroute-results"}

# Scamper command
SCAMPER=${SCAMPER-"scamper -c trace"}

# Destination
DESTHOST=${DESTHOST-"tor-traceroutes@ttat-control.iti.illinois.edu"}

# File containing the start and end IP of each routable prefix obtained from Route-Views project (http://www.routeviews.org/)
PREFIX_FILE=${PREFIX_FILE-prefixIP.txt}

# File containing the list of Tor relay IPs dynamically downloaded (default relays seen on April 14, 2014)
RELAY_FILE=${RELAY_FILE-"relay-ips.txt"}

# Temp File containing the list of Tor relay IPs
TEMP_FILE=${TEMP_FILE-"temp-ips.txt"}

# Make a directory to store the results
mkdir -p "$RESULTS" && cd "$RESULTS" || exit

compsuffix=.Z
if type bzip2 &>/dev/null; then
    compress () { bzip2 -f ; }
    compsuffix=.bz2
elif type gzip &>/dev/null; then
    compress () { gzip -f ; }
    compsuffix=.gz
elif ! type compress &>/dev/null; then
    # BSD compress doesn't exist, send things
    # uncompressed
    compress () { cat ; }
    compsuffix=
fi

if [[ ! -z "${DONTERASE+yes}" ]]; then
    # Never erase anything
    rm () { : ; }
fi
if [[ ! -z "${DONTUPLOAD+yes}" ]]; then
    # Don't upload anything
    ssh () { cat >/dev/null ; }
fi


function method_startset {
   count=0  #current number of traceroutes
   curdirnum=0 #current wokring set of DIRSIZE traceroutes
   rm -f *.ips # Clear the IP file if it happens to exist
}

function run_scamper {
	eval $SCAMPER -p $PPS -f $prefix-$curdirnum.ips | filter >> $prefix-$curdirnum.scamper
	cat $prefix-$curdirnum.scamper | compress  | ssh -F ../ssh-config $DESTHOST $prefix-$curdirnum.scamper-prefixIPselection-subnetmask-$1-$PPS-$VERSION$compsuffix
    rm -f $prefix-$curdirnum.ips  # remove the IP file
    rm -f $prefix-$curdirnum.scamper #remove results
    ((++curdirnum))
}

function process_ip {
    echo $1 >> $prefix-$curdirnum.ips
}


function startset {
    prefix=$1-$(date +%s)-$(hostname)
    if [[ $2 -eq 0 ]]; then
        function filter { sed -e 's/ *[0-9.]* ms//g' ; }
    else
        function filter { cat ; }
    fi
    method_startset
}


##Try first 50 IPs sequentially and then 50 random IPs for each prefix
function IPselection {
	ipselected=0;
	while [ $ipselected -lt $MAXSEQ ]; do
		ip=$(($1+ipselected))
		process_ip $ip
		((ipselected++))
	done
	
	ipselected=0;
	while [ $ipselected -lt $MAXSEQ ]; do
		ip=$(($1+$RANDOM%$2))
		process_ip $ip
		((ipselected++))
	done
}


## Routeviews prefixes
startset routeviews 0

while IFS="|" read start_ip end_ip subnet; do
	#Compute the range (in decimal format) of a prefix
	range=$((end_ip - start_ip + 1))

	if [ $range -eq 0 ]; then
		ip=$start_ip #only one feasible address special case for /31 prefixes
		process_ip $ip 
	elif [ $range -lt 0 ]; then
		ip=$((start_ip-1)) #use the exact IP for /32 prefixes
		process_ip $ip 
	else
		#ip=$((start_ip+$RANDOM%range)) #Randomly choose an IP from a prefix
		IPselection $start_ip $range
	fi
	run_scamper $subnet
done < ../$PREFIX_FILE


## Tor relays, record latency information
startset relays 1

##Download latest relay ips and replace file only if file size greater than zero
rm -f ../$TEMP_FILE
curl -o ../$TEMP_FILE http://torstatus.blutmagie.de/ip_list_all.php/Tor_ip_list_ALL.csv > /dev/null 2>&1 
if [[ -s ../$TEMP_FILE ]] ; then
	cat ../$TEMP_FILE | sort -R > ../$RELAY_FILE
fi

while read ip; do
	process_ip $ip
done < ../$RELAY_FILE
run_scamper 0


cd ..
rm -r "$RESULTS"
