#########################################################################
# Traceroutes to random IP addresses                                    #
#       by                                                              #
#       Anupam Das, Nikita Borisov                                      #
#       Hatswitch Group                                                 #
#       UIUC                                                            #
#       Date- September 23 2013 					#
#									#
#	Email to  tor-traceroutes@illinois.edu                     	#
#									#
#########################################################################


Pre-parsed Files:
____________________________

1. The file titled 'prefix.txt' (about 10MB in size) contains routeview's publicly
routable IP ranges. The data was obtained from
                http://archive.routeviews.org/bgpdata/2013.09/RIBS/
Details about how data was obatined and parsed is available in the directory titled
"Routeview-PrefixRetrieval".

2. The file titled 'relay-ips.txt' contains all Tor relay IPs that appeared during
the period 9/19/13-9/25-13. These information can be retrieve from the Tor Metrics
Portal available at https://metrics.torproject.org/data.html#relaydesc

3. The file titled 'allowed-ips.txt' contains all the allowed IP ranges
(start and end ip in decimal format) based on the blocked/reserved IP list obtained from
https://tools.ietf.org/html



Pre-installations:
________________________________

This tool requires the following installations before it can be executed-

1. tar (needed for archiving data)
        sudo apt-get install tar   or  sudo yum install tar

2. ssh client (needed for sending data)
        Most OSes usually have this preintalled
         
3. To run traceroutes accurately (using Paris-traceroute
   with UDP packets) we encourage you to install "scamper", a network tool,
   developed by CAIDA: http://www.caida.org/tools/measurement/scamper/

   Scamper is available in FreeBSD ports, NetBSD pkgsrc, OpenBSD ports, and in
   Debian/Ubuntu packages. The FreeBSD, NetBSD, and OpenBSD packages should be up
   to date with the latest version of scamper.

   In case you can't find the package the latest source code is available at-
   http://www.caida.org/tools/measurement/scamper/code/scamper-cvs-20130824.tar.gz
   (http://www.caida.org/tools/measurement/scamper/code/)

   If your OS does not have a scamper package, you can install it from source:

   wget http://www.caida.org/tools/measurement/scamper/code/scamper-cvs-20130824.tar.gz
   tar -xaf scamper-cvs-20130824.tar.gz
   cd scamper-cvs-20130824
   ./configure
   make
   sudo make install

    (Scamper will need elevated privileged to create sockets. So scamper has to
    be installed with setuid root)
    
    sudo chmod 4755 /path/to/scamper

    If scamper is installed, the traceroutes script will use it by default instead
    of traceroute.

4.  If you don't want to install scamper, please make sure the basic 'traceroute' 
    is installed. Most OSes usually have this preintalled (except for Ubuntu).
    
    To install traceroute on Ubuntu/Fedora run the following command:
    sudo apt-get install traceroute   or  sudo yum install traceroute


Running script:
________________________________

The script titled 'traceroutes.sh' runs traceroutes to a randomly choosen IP
address and by default prints only the IP address of each hop. No latency information
is recorded except for traceroutes to other Tor relays. We did not remove latency information
for traceroutes to Tor relays because anyone can compute these latencies by creating circuits
through those Tor relays. The script first creates a directory to store the traceroute
responses and at the end compresses files before sending to a designated host. Once data is 
sent the files, by default are deleted.

The script runs traceroutes to the following destinations

1. A random IP in each publicly routable (~500K) IP prefix obtained from Route-View project.
2. Tor relays
3. A random IP in each /24 prefix


The script has the following Configurable parameters and default values:

# Number of parallel traceroutes that will run
PARALLEL=${PARALLEL-128}

# Scamper packets per second rate 1<=pps<=1000
PPS=${PPS-1000}

# Method of traceroute
METHOD= traceroute or scamper

# How many traceroute responses per directory 
DIRSIZE=65536 


You can also choose not to upload/erase the data obtained from the traceroutes. By default the script will 
upload and erase data. There is also a debug option which lists the total number of traceroutes runnings and
the total time required to run one batch of traceroutes.

#If you don't want to erase result after sending data
DONTERASE=yes

#If you don't want to upload data
DONTUPLOAD=yes

#If you want to debug how long traceroutes will take
DEBUG=yes

Please don't erase data without uploading the data, otherwise the whole purpose of doing 
traceroute is meaningless.


The script can be executed in the following modes:

./traceroutes.sh &    (uses all the default settings)

DONTERASE=yes ./traceroutes.sh &    (will not erase data)

DONTUPLOAD=yes ./traceroutes.sh &    (will not upload data)

METHOD=traceroute ./traceroutes.sh &   (force traceroute even if scamper is installed)

PPS=500 ./traceroutes.sh &   (set scamper's packet-per-second rate to 500)

DEBUG=yes ./traceroutes.sh & (debug mode is selected, only useful for basic traceroute method)

You can also use combinations of different parametric setting like-

METHOD=traceroute DONTERASE=yes ./traceroutes.sh &

(you can also change the destination directory in the script if you want,
by default it will create a directory titled 'Traceroute-results' under
the same directory where the script lies. Files titled 'prefix.txt', 'relay-ips.txt'
and 'allowed-ips.txt' must reside in the same directory as the script.)


Data Upload:
________________________________

Data are sent to the following server-

tor-traceroutes@ttat-control.iti.illinois.edu

If you run the script with DONTERASE=yes, you will have to manually send us the results.
It would be very helpful if you could send the results of each phase as it completes.
The results from each phase are stored in files (or subdirectories) inside Traceroute-results as follows:
 1. The first-phase directories begin with the string "routeviews-".
 2. The second-phase directories begin with the string "relays-".
 3. The third-phase directories begin with the string "slash24-".
The files/directories of a phase don't begin to be created until the previous phase has
completed. Thus, for example, when you start seeing "relays-" files/directories, the first phase
has completed.

To send us results in files/directories Traceroute-results/$phase-$date-$host-* please ssh them
to the server with the following command while in the Traceroute-results directory:

  tar cf - $phase-$date-$host-* | gzip -f | ssh -F ../ssh-config tor-traceroutes@ttat-control.iti.illinois.edu $phase-$date-$host.traceroute-$PARALLEL/$PPS-$VERSION.tar.gz
(If you want to use 'gzip' as the compression utility)

 tar cf - $phase-$date-$host-* | bzip2 -f | ssh -F ../ssh-config tor-traceroutes@ttat-control.iti.illinois.edu $phase-$date-$host.traceroute-$PARALLEL/$PPS-$VERSION.tar.bz2
(If you want to use 'bzip2' as the compression utility)

where
  - ssh-config is a file included in the repository that points ssh to the server key,
  - $phase, $date, and $host appear in the directory name (e.g., routeviews-1382753222-AnupamHP)

  - $PARALLEL is the setting you used for PARALLEL when running the script (128 is the
    default used if you didn't set it explicitly)
  - $PPS is the setting you used for PPS when running the script with scamper(1000 is the
    default used if you didn't set it explicitly)
   
    (Either use 'PPS' or 'PARALLEL' but not both based on whether you run scamper or traceroute utility)

  - $VERSION is the version of the code you used (1.1.4 is the default and the version of
    commit f253f768d14e3368e4fe4de9895acd2715a19412).


Sample command:

cd Traceroute-results 

tar cf - routeviews-1382753222-AnupamHP-* | bzip2 -f | ssh -F ../ssh-config tor-traceroutes@ttat-control.iti.illinois.edu \
routeviews-1382753222-AnupamHP.traceroute-128-1.1.4.tar.bz2



Resource and Time requirement:
________________________________

Detailed description of different resouces requirements (like bandwidth, disk space, CPU and RAM) along with 
total run-time is provided at-

http://web.engr.illinois.edu/~das17/tor-traceroute_v1.html



Live Scoreboard:
________________________________

We have a live scoreboard of the current participants available at- http://128.174.241.211:443/relay_scoreboard


Contact :
____________________

If you have any question or concerns please send your questions to tor-traceroutes@illinois.edu
We will respond as early as possible.






